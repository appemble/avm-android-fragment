/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.fragment.activities;

import java.util.HashMap;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Display;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.HttpUtils;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.ADD_FROM_CONTACT;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dynamicapp.ActivityData;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.dynamicapp.DynamicActivity;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.ControlDao;
import com.appemble.avm.models.dao.ScreenDao;
import com.appemble.avm.models.dao.TableDao;
import com.loopj.android.http.AsyncHttpClient;

public class DynamicFragmentActivity extends FragmentActivity implements AppembleActivity {
	Context context = this;
	View activityLayout = null;
	DynamicLayout bodyLayout = null;
	DynamicLayout titleLayout = null;
	boolean bLayoutAttempted = false;
	boolean isNetworkAvailable = true;
	boolean isDestroyed = false;
	boolean bBeingCreated = false;
	boolean bBeingRecreated = false, isBeingDestroyedForConfigChanges = false;
	boolean bOrientationRequested = false;
	public ActivityData mActivityData;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {		
		if (null == Cache.context && false == Cache.initiate(this.getApplicationContext(), true))
			return;
		super.onCreate(savedInstanceState);
		if (null == mActivityData) {
		    mActivityData = (ActivityData) getLastNonConfigurationInstance();
		    if (null == mActivityData)
		    	mActivityData = new ActivityData();
		    else
		    	bBeingRecreated = true;
		}
	    // A dynamic activity needs to have a associated screen name.
		// screeName could be set in the DynamicSplashActivity also.
		if (null == mActivityData.sScreenName && getIntent() != null && getIntent().getExtras() != null)
			mActivityData.sScreenName = getIntent().getExtras().getString(Constants.STR_SCREEN_NAME);			
		String sSystemDbName = null, sContentDbName = null; 
		// sDbName could be set in the DynamicSplashActivity also.
		if (getIntent() != null && getIntent().getExtras() != null)
			sSystemDbName = getIntent().getExtras().getString(Constants.STR_SYSTEM_DATABASE_NAME);
		else
			sSystemDbName = Cache.bootstrap.getValue(Constants.STR_SYSTEM_DATABASE_NAME);
		if (getIntent() != null && getIntent().getExtras() != null)
			sContentDbName = getIntent().getExtras().getString(Constants.STR_CONTENT_DATABASE_NAME);
		else
			sContentDbName = Cache.bootstrap.getValue(Constants.STR_CONTENT_DATABASE_NAME);

		if (sSystemDbName == null || null == sContentDbName) {
			LogManager.logError("Error in finding system database. Please check res/values/strings.xml for system_dbname and content_dbname. Make sure they are present in the assets folder. Aborting...");
			finish();
			return;
		}
		if (null == mActivityData.screenModel) { // activities like splash activity may have fetched it.
			if (null == mActivityData.sScreenName || mActivityData.sScreenName.length() == 0)
				mActivityData.screenModel = ScreenModel.getScreenModel(sSystemDbName, sContentDbName, null);
			else
				mActivityData.screenModel = ScreenDao.getScreenByName(sSystemDbName, sContentDbName, mActivityData.sScreenName);
		}
		if (null == mActivityData.screenModel) {
			LogManager.logError("Error in fetching data for screen: " + mActivityData.sScreenName + ". Aborting...");
			finish();
			return;
		} else {
			String sClassName = mActivityData.screenModel.getClassName();
			if (!(this.getClass().getName().equals(sClassName))) {
				finish();
				return;
			}
		}
		if (bOrientationRequested == false) {
			bOrientationRequested = true;
			int iCurrentOrientation = getResources().getConfiguration().orientation;
			if (mActivityData.screenModel.iAllowedLayouts != Configuration.ORIENTATION_UNDEFINED) {
				setRequestedOrientation(mActivityData.screenModel.iAllowedLayouts == Configuration.ORIENTATION_LANDSCAPE ? 
						ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
				if ((mActivityData.screenModel.iAllowedLayouts != iCurrentOrientation))
					return; // the setRequestedOrientation will result is re-creation of the activity.
			} else {
				// if allow_reorientation
				if (mActivityData.screenModel.bAllowReorientation) { // if allow re-orientation
					setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
//					return;
				}
				else				
					setRequestedOrientation(iCurrentOrientation == Configuration.ORIENTATION_LANDSCAPE ?
						ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE : ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
			}
		}
		bBeingCreated = true; isDestroyed = false;
		boolean bNetworkConnectionRequired = Utilities.parseBoolean(
				mActivityData.screenModel.mExtendedProperties.get(Constants.STR_REQUIRES_NETWORK_CONNECTION));
		if (bNetworkConnectionRequired && false == AppembleActivityImpl.isNetworkAvailable(this)) {
			AppembleActivityImpl.showAlertAndCloseScreen(this, "Alert...", Constants.STR_NETWORK_CONNECTION_REQUIRED);
			return;
		}
		long lCreateTime = 0;
		if (Constants.isDebugMode)
			lCreateTime = System.currentTimeMillis();
		if (false == bBeingRecreated)
			mActivityData.targetParameterValueList = getIntent().getExtras();
		if (null == mActivityData.targetParameterValueList)
			mActivityData.targetParameterValueList = new Bundle();
		LogManager.logVerbose("Creating screen: " + mActivityData.sScreenName);
		if (null == mActivityData.vControls)
			mActivityData.vControls = ControlDao.getAllControlsinScreen(sSystemDbName, sContentDbName, 
				mActivityData.screenModel.sName);
		if (null == mActivityData.vControlsInScreenAndTitle) {
			mActivityData.vControlsInScreenAndTitle = new Vector<ControlModel> ();
			AppembleActivityImpl.getChildControls(mActivityData.vControls, Constants.SCREEN_ID, 
					mActivityData.vControlsInScreenAndTitle);
			AppembleActivityImpl.getChildControls(mActivityData.vControls, Constants.TITLE_ID, 
					mActivityData.vControlsInScreenAndTitle);
		}
		if (false == bBeingRecreated)
			Action.callScreenActions(this, DynamicActivity.BEFORE_ON_CREATE_SCREEN, 
				null, null, mActivityData.targetParameterValueList);

		if (null == mActivityData.vControlsInScreenAndTitle || mActivityData.vControlsInScreenAndTitle.size() == 0)
			return; // log error here...

		if (false == createLayout())
			LogManager.logWTF("Unable to create layout for screen: " + mActivityData.sScreenName);
		if (Constants.isDebugMode) {
			lCreateTime = System.currentTimeMillis() - lCreateTime;
			LogManager.logDebug("Screen " + mActivityData.screenModel.sName + " got created in " + lCreateTime + "ms. ");
		}
		if (false == bBeingRecreated)
			Action.callScreenActions(this, DynamicActivity.ON_CREATE_SCREEN, 
				null, null, mActivityData.targetParameterValueList);

		return; 
	}

	@Override
	public void onStart() {
        super.onStart();
        if (false == bBeingRecreated)
        	Action.callScreenActions(this, Constants.ON_START_SCREEN, 
        		null, null, mActivityData.targetParameterValueList);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (false == bBeingRecreated) { // If the screen is not being recreated, then try to close screens if needed or generate the event ON_APPLICATION_FOREGROUND
			if (AppembleActivityImpl.closeScreens(this)) {
//				super.onResume();
				return;
			}
	// Not used Method 1: http://stackoverflow.com/questions/4414171/how-to-detect-when-an-android-app-goes-to-the-background-and-come-back-to-the-fo
			if (mActivityData.screenModel != null && 
					Cache.isApplicationInBackground == true) {
				Cache.isApplicationInBackground = false;
				// Application is coming in foreground
				ActionModel[] actions = ActionModel.getActions(mActivityData.screenModel.sSystemDbName, 
						mActivityData.screenModel.sContentDbName, DynamicActivity.ON_APPLICATION_FOREGROUND);
			    for (int i = 0; null != actions && i < actions.length; i++)
			    	Action.callAction(this, null, null, actions[i], mActivityData.targetParameterValueList);
			}
			Action.callScreenActions(this, DynamicActivity.BEFORE_ON_RESUME_SCREEN, 
				null, null, mActivityData.targetParameterValueList);
		}
		if (bBeingRecreated) {
			if (Cache.cookieSyncManager != null)
				Cache.cookieSyncManager.startSync();
		}
		// if for some reason the body layout did not get created, 
		// no point in deparameterizeing and updating controls with local or remote data
		long lUpdateTime = 0;
		if (null == bodyLayout)
			return; 
		if (Constants.isDebugMode)
			lUpdateTime = System.currentTimeMillis();
	    // Call Activity.onResume
//		if (false == bBeingRecreated) // deparameterize data source only once. Have to do as deparameterized local / remote data source will be null if not done so
			AppembleActivityImpl.deparameterizeDataSources(mActivityData.screenModel, mActivityData.vControls, 
	    		mActivityData.targetParameterValueList, mActivityData.screenModel.sContentDbName); // substitute <parameter> with value in targetParameterValueList
		LogManager.logVerbose("Resuming screen: " + mActivityData.sScreenName + ". \r\n With Local Data Source is : " + 
				mActivityData.screenModel.sDeparameterizedLocalDataSource + 
				". \r\n And Remote Data Source is : " + mActivityData.screenModel.sDeparameterizedRemoteDataSource);
		// Call onResume function for every view in the screen.
		AppembleActivityImpl.onScreenStateChange(Constants.TITLE_ID, titleLayout, mActivityData.vControls, 
				mActivityData.targetParameterValueList, Constants.ON_RESUME_SCREEN);
		AppembleActivityImpl.onScreenStateChange(Constants.SCREEN_ID, bodyLayout, mActivityData.vControls, 
				mActivityData.targetParameterValueList, Constants.ON_RESUME_SCREEN);
		if (bBeingCreated || bBeingRecreated || mActivityData.screenModel.bOnResumeUpdate) {
			if (bBeingCreated)
			    AppembleActivityImpl.setDefaultValues(activityLayout);
		    boolean bIsRemoteDataPresent = false;
			if (false == bBeingRecreated)
				bIsRemoteDataPresent = AppembleActivityImpl.isRemoteDataPresent(mActivityData.screenModel, 
					mActivityData.vControls);
			if (!bIsRemoteDataPresent) {
				if (false == bBeingCreated && mActivityData.cursors.size() > 0) {
					LogManager.logVerbose("Cursors should be not be present at the start of the Activity.onResume");
					mActivityData.cursors.clear();
				}
				// fetch and update local data.
				AppembleActivityImpl.fetchLocalData(this, null, true); 
				// populate the screen with local data. 
				if (false == AppembleActivityImpl.update(this, null, null)) {
					bBeingCreated = false;
					return; // if the function return 0 means there is an error while executing the function.
				}
				if (Constants.isDebugMode) {
					lUpdateTime = System.currentTimeMillis() - lUpdateTime;
					LogManager.logDebug("Local data update: " + lUpdateTime + "ms");
				}
				bBeingCreated = false;
			} else {
				// Fetch remote data in the background thread and then use UI thread to update the UI. It is part of lazy loading...
				if (false == bBeingRecreated) // do not call remote data if being called because of recreation of the activity due to orientation change 
					isNetworkAvailable = AppembleActivityImpl.updateRemoteData(this, null, true, null, isNetworkAvailable,
							bBeingCreated);
				else // do not update bBeingCreated if RemoteDataIsPresent. It will be set to false in onFinish() 
					bBeingCreated = false;
			}
		}
		if (false == bBeingRecreated)
			Action.callScreenActions(this, Constants.ON_RESUME_SCREEN, null, 
				activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
	}

	@Override
	protected void onPostResume() {
// Not Used Method 1: http://stackoverflow.com/questions/4414171/how-to-detect-when-an-android-app-goes-to-the-background-and-come-back-to-the-fo
		super.onPostResume();
		if (null == mActivityData.screenModel)
			return;
		if (false == bBeingRecreated)
			Action.callScreenActions(this, DynamicActivity.ON_POST_RESUME_SCREEN, null, 
				activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
		else {	
			if (null != mActivityData && null != mActivityData.mScreenData)
				mActivityData.mScreenData.clear(); // clear any store data.
			bBeingRecreated = false;
			Action.callScreenActions(this, DynamicActivity.POST_ORIENTATION_CHANGE, 
				null, activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
		}
	}
	
	@Override
	public void onPause() {
		super.onPause();
		Cache.isApplicationInBackground = Utilities.isApplicationSentToBackground(this);
// Not Used Method 1: http://stackoverflow.com/questions/4414171/how-to-detect-when-an-android-app-goes-to-the-background-and-come-back-to-the-fo		
    	if (Cache.cookieSyncManager != null)
    		Cache.cookieSyncManager.stopSync();
    	if (null == mActivityData.screenModel)
    		return;
		AppembleActivityImpl.onScreenStateChange(Constants.TITLE_ID, titleLayout, mActivityData.vControls, 
				mActivityData.targetParameterValueList, Constants.ON_PAUSE_SCREEN);
		AppembleActivityImpl.onScreenStateChange(Constants.SCREEN_ID, bodyLayout, mActivityData.vControls, 
				mActivityData.targetParameterValueList, Constants.ON_PAUSE_SCREEN);
		// Cannot detect if onPause is called due to activity getting destroyed because of orientation change. 
	    if (isFinishing())
			Action.callScreenActions(this, DynamicActivity.BEFORE_ON_DESTROY_SCREEN, null, 
				activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
	    else
			Action.callScreenActions(this, Constants.ON_PAUSE_SCREEN, null, 
				activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
	}

	@Override
	public void onStop() {
		super.onStop();		
		if (false == isBeingDestroyedForConfigChanges)
			Action.callScreenActions(this, Constants.ON_STOP_SCREEN, null, 
				activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		isDestroyed = true;
		// cancel all pending remote requests if the activity is being destroyed.
		AsyncHttpClient client = HttpUtils.getAsyncHttpClient();
		if (null != client)
			client.cancelRequests(this, true);
		if (null == mActivityData.screenModel)
			return;
		bOrientationRequested = false; bLayoutAttempted = false;
		
		if (false == isBeingDestroyedForConfigChanges) {
			LogManager.logVerbose("Destroying screen: " + mActivityData.screenModel.sName);
			Action.callScreenActions(this, Constants.ON_DESTROY_SCREEN, null, 
				activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
		}
		// Call onResume function for every view in the screen.
		AppembleActivityImpl.onScreenStateChange(Constants.TITLE_ID, titleLayout, mActivityData.vControls, 
				mActivityData.targetParameterValueList, Constants.ON_DESTROY_SCREEN);
		AppembleActivityImpl.onScreenStateChange(Constants.SCREEN_ID, bodyLayout, mActivityData.vControls, 
				mActivityData.targetParameterValueList, Constants.ON_DESTROY_SCREEN);
		activityLayout = null; bodyLayout = null; titleLayout = null;
		isBeingDestroyedForConfigChanges = false; // set it to the default value. Next time it may be set to true.
	}
	
	@Override
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);
		if (reqCode == ADD_FROM_CONTACT.iOnPickContact) {
			if (resultCode == Activity.RESULT_OK && (resultCode = 
				Utilities.getContactInfo(this, data, mActivityData.targetParameterValueList)) == Activity.RESULT_OK) {
				Action.callScreenActions(this, ADD_FROM_CONTACT.iOnPickContact, null, 
						activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
			}
		} else {
			mActivityData.targetParameterValueList.putParcelable("data", data);
			mActivityData.targetParameterValueList.putInt("activity_result_code", resultCode);
			Action.callScreenActions(this, reqCode, null, 
				activityLayout != null ? activityLayout : bodyLayout, 
				false, mActivityData.targetParameterValueList);
		}
	}
	
//	@Override
//	public boolean onTouchEvent(android.view.MotionEvent event) {
//		Action.callScreenActions(this, Constants.TAP, null, activityLayout != null ? activityLayout : bodyLayout);
//		return false;		
//	};
//	
    final GestureDetector.SimpleOnGestureListener listener = new GestureDetector.SimpleOnGestureListener() {
        @Override
        public boolean onSingleTapUp(MotionEvent e) {
            Action.callScreenActions(DynamicFragmentActivity.this, Constants.TAP, null, 
            	activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
            return true;
        }
        
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            Action.callScreenActions(DynamicFragmentActivity.this, Constants.DOUBLE_TAP, null, 
            	activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
            return true;
        }
  
        @Override
        public void onLongPress(MotionEvent e) {
            Action.callScreenActions(DynamicFragmentActivity.this, Constants.LONG_PRESS, 
            	null, activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
        }
    };
  
	@Override
	public Vector<ControlModel> getAllControls() {
		return mActivityData.vControls;
	}	

	@Override
	public ScreenModel getScreenModel() { 
		return mActivityData.screenModel;
	}
	
	@Override
	public Bundle getTargetParameterValueList() {
		return mActivityData.targetParameterValueList;
	}
	
	@Override
	public boolean getActivityCreateStatus() {
		return bBeingCreated;
	}
	
	@Override
	public boolean getNetworkAvailableFlag() {
		return isNetworkAvailable;
	}
	
	public synchronized boolean createLayout() {
		if (bLayoutAttempted)
			return true;
		bLayoutAttempted = true;
		LinearLayout rootLayout = new LinearLayout(context); // ActivityLayout holds both title and body layouts
		LinearLayout.LayoutParams activityParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT
		        ,ViewGroup.LayoutParams.FILL_PARENT);
		rootLayout.setLayoutParams(activityParams);
		
		Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		//if (Integer.valueOf(android.os.Build.VERSION.SDK) < 13) {
		mActivityData.screenModel.fWidthInPixels = display.getWidth();
		mActivityData.screenModel.fHeightInPixels = display.getHeight();
		//} else
		//	display.getSize(size);
			
		mActivityData.screenModel.fHeightInPixels -= Cache.iStatusBarHeight; // decrease the height of the status bar.
		float fTabBarHeight = (mActivityData.screenModel.iDimensionType == Constants.PIXELS ? 
			mActivityData.screenModel.fTabBarHeight : 
				(mActivityData.screenModel.fTabBarHeight * mActivityData.screenModel.fHeightInPixels) / 100);
		mActivityData.screenModel.fHeightInPixels -= fTabBarHeight; // decrease the height of the tab bar if present in a tabbed activity
		if (Constants.iOS == 4) // amazon
			mActivityData.screenModel.fHeightInPixels -= Cache.getKindleSoftKeyBarHeight(); // decrease hard coded 20px for kindle products.
		mActivityData.screenModel.fWidthInPixels = (mActivityData.screenModel.iDimensionType == Constants.PIXELS ? 
			mActivityData.screenModel.fWidth : 
				(mActivityData.screenModel.fWidth * mActivityData.screenModel.fWidthInPixels) / 100); 
		mActivityData.screenModel.fHeightInPixels = (mActivityData.screenModel.iDimensionType == Constants.PIXELS ? 
				mActivityData.screenModel.fHeight : 
					(mActivityData.screenModel.fHeight * mActivityData.screenModel.fHeightInPixels) / 100);
        if (mActivityData.screenModel.iControlHeightBasedUpon != Configuration.ORIENTATION_UNDEFINED)
          mActivityData.screenModel.fHeightInPixels = mActivityData.screenModel.iControlHeightBasedUpon == Configuration.ORIENTATION_PORTRAIT ? 
              (mActivityData.screenModel.fHeightInPixels > mActivityData.screenModel.fWidthInPixels ? mActivityData.screenModel.fHeightInPixels : mActivityData.screenModel.fWidthInPixels) : 
              (mActivityData.screenModel.fHeightInPixels < mActivityData.screenModel.fWidthInPixels ? mActivityData.screenModel.fHeightInPixels : mActivityData.screenModel.fWidthInPixels);
		AppearanceManager.getInstance().setInitialWindowHeight(mActivityData.screenModel.iAllowedLayouts, 
				mActivityData.screenModel.fWidthInPixels, mActivityData.screenModel.fHeightInPixels);
		
		final Window window = getWindow();	    
		// Create title first
		Vector<ControlModel> vControlsInTitle = new Vector<ControlModel> ();
		AppembleActivityImpl.getChildControls(mActivityData.vControls, Constants.TITLE_ID, vControlsInTitle);
		if (vControlsInTitle.size() > 0) {
			window.requestFeature(Window.FEATURE_NO_TITLE); // remove the default title
		    titleLayout = new DynamicLayout(context);
			// if (false == titleLayout.generateLayout(Constants.TITLE, Constants.TITLE_ID, screenModel, vControls))
			if (false == DynamicLayout.generateLayout(titleLayout, Constants.TITLE_ID, mActivityData.screenModel.fWidthInPixels,
					mActivityData.screenModel.fHeightInPixels, mActivityData.screenModel, mActivityData.vControls))
				return false; // Log error here
			LinearLayout.LayoutParams titleParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT
			        ,ViewGroup.LayoutParams.WRAP_CONTENT);
			titleLayout.setLayoutParams(titleParams);
			String sTitleBackground = mActivityData.screenModel.sTitleBackground != null ? mActivityData.screenModel.sTitleBackground :
				TableDao.getColumnValue(mActivityData.screenModel.sSystemDbName, "_screen_deck", "title_background", null);
			AppembleActivityImpl.setBackground(titleLayout, sTitleBackground);
			if (null != sTitleBackground) {
				if(sTitleBackground.startsWith("#")) {
					titleLayout.setBackgroundColor(Color.parseColor(sTitleBackground));
				}else{
//					ImageManager.getInstance().UpdateView(titleLayout, sTitleBackground, true);
					AppearanceManager.getInstance().updateBackground(titleLayout, sTitleBackground);
				}
			} else
				titleLayout.setBackgroundColor(Color.parseColor("#0000FF")); // blue color
			rootLayout.addView(titleLayout);
			mActivityData.screenModel.fHeightInPixels -= AppembleActivityImpl.getCustomTitleBarHeight(vControlsInTitle, 
				mActivityData.screenModel.fHeightInPixels);
		} else if (0 == mActivityData.screenModel.fTabBarHeight) {
			if (mActivityData.screenModel.iDefaultTitleBar == 0 || (mActivityData.screenModel.iDefaultTitleBar == -1 && 
					Cache.iDefaultTitleBar == 0))
				window.requestFeature(Window.FEATURE_NO_TITLE);
			else {// reduce the default title bar height.
				mActivityData.screenModel.fHeightInPixels -= AppembleActivityImpl.getDefaultTitleBarHeight(this);
			}
		    AppearanceManager.getInstance().setInitialWindowHeight(mActivityData.screenModel.iAllowedLayouts, 
		    		mActivityData.screenModel.fWidthInPixels, mActivityData.screenModel.fHeightInPixels);
		}
		// now create the body of the screen
		bodyLayout = new DynamicLayout(context);
		//if (titleLayout != null) This is too soon for titleLayout to return the height. It will always return 0.
		//	screenModel.iHeight = rScreenRect.bottom -= titleLayout.getHeight();
		// if (false == bodyLayout.generateLayout(Constants.BODY, Constants.SCREEN_ID, 
		if (false == DynamicLayout.generateLayout(bodyLayout, Constants.SCREEN_ID, mActivityData.screenModel.fWidthInPixels, 
				mActivityData.screenModel.fHeightInPixels, mActivityData.screenModel, mActivityData.vControls))
			return false; // Log error here
	
		String sBackground = mActivityData.screenModel.sBackground != null ? mActivityData.screenModel.sBackground :
			TableDao.getColumnValue(mActivityData.screenModel.sSystemDbName, "_screen_deck", "background", null);
		AppembleActivityImpl.setBackground(bodyLayout, sBackground);
		// Set the orientation
		rootLayout.setOrientation(LinearLayout.VERTICAL); // The body layout should always be below the titleLayout.

		// Add the body layout
		if (bodyLayout != null) {
			HorizontalScrollView hsv = null;
			if (mActivityData.screenModel.iScrolling == Constants.HSCROLL || mActivityData.screenModel.iScrolling == Constants.SCROLL_BOTH_WAYS) {
				hsv = new HorizontalScrollView(this);
				hsv.addView(bodyLayout);
			}
			ScrollView vsv = null;
			if (mActivityData.screenModel.iScrolling == Constants.VSCROLL || 
					mActivityData.screenModel.iScrolling == Constants.SCROLL_BOTH_WAYS) {
				vsv = new ScrollView(this);
				if (null != hsv)
					vsv.addView(hsv);
				else
					vsv.addView(bodyLayout);
			}
			if (null != vsv) {
				rootLayout.addView(vsv);
				activityLayout = rootLayout;
			}
			else if (null != hsv) {
				rootLayout.addView(hsv);
				activityLayout = rootLayout;
			}
			else
				activityLayout = bodyLayout;
		}
		
		setContentView(activityLayout);
		
		if (mActivityData.screenModel.sInitialFocus != null) {
			View vInitialFocus = AppembleActivityImpl.findViewInScreen(this, 
					AppembleActivityImpl.findControlModelByName(this, mActivityData.screenModel.sInitialFocus));
			if (null != vInitialFocus) {
				vInitialFocus.setFocusableInTouchMode(true);
				vInitialFocus.requestFocus();
			}
		}

		return true;
	}
	
	@Override
	public Context getContext() {
		return this;
	}

	@Override
	public HashMap<Integer, Cursor> getCursors() {
		return mActivityData.cursors;
	}

	@Override
	public View getRootLayout() {
		return activityLayout;
	}

	@Override
	public void setActivityCreateStatus(boolean bCreated) {
		bBeingCreated = bCreated;
	}

	@Override
	public boolean getActivityRecreateStatus() {
		return bBeingRecreated;
	}

	@SuppressLint("Override")
	public boolean isDestroyed() {
		return isDestroyed;
	}

	public Activity getMyParent() {
		return getParent();
	}

	public ActivityData getActivityData() {
		return mActivityData;
	}	
}