package com.appemble.fragment.activities;

import java.util.HashMap;
import java.util.Vector;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.appemble.avm.AppearanceManager;
import com.appemble.avm.Cache;
import com.appemble.avm.Constants;
import com.appemble.avm.HttpUtils;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.Action;
import com.appemble.avm.dynamicapp.ActivityData;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.dynamicapp.DynamicActivity;
import com.appemble.avm.dynamicapp.DynamicLayout;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.ControlDao;
import com.appemble.avm.models.dao.ScreenDao;
import com.appemble.avm.models.dao.TableDao;
import com.loopj.android.http.AsyncHttpClient;

public final class DynamicFragment extends Fragment implements AppembleActivity {
    public float fWidth, fHeight;
    LinearLayout activityLayout = null;
    DynamicLayout bodyLayout = null;
    DynamicLayout titleLayout = null;
    boolean bBeingCreated = false;
    boolean bBeingRecreated = false, isBeingDestroyedForConfigChanges = false;
    boolean isDestroyed = false;
    boolean isNetworkAvailable = true;
    public ActivityData mActivityData;
    
    public static DynamicFragment newInstance(ScreenModel screenModel, float fWidth, float fHeight) {
        DynamicFragment fragment = new DynamicFragment();
        fragment.mActivityData = new ActivityData();
        fragment.mActivityData.screenModel = screenModel;
        fragment.fWidth = fWidth;
        fragment.fHeight = fHeight;

        return fragment;
    }    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mActivityData = new ActivityData();
            String sSystemDbName = savedInstanceState.getString("SystemDbName");
            String sContentDbName = savedInstanceState.getString("ContentDbName");
            mActivityData.sScreenName = savedInstanceState.getString("ScreenName");
            mActivityData.screenModel = ScreenDao.getScreenByName(sSystemDbName, sContentDbName, mActivityData.sScreenName);
            mActivityData.mScreenData = savedInstanceState.getBundle("ScreenData");
            mActivityData.targetParameterValueList = savedInstanceState.getBundle("TargetParameterValueList");
            mActivityData.vControlsInScreenAndTitle = new Vector<ControlModel> ();
            fWidth = savedInstanceState.getFloat("Width");
            fHeight = savedInstanceState.getFloat("Height");
                
            mActivityData.vControls = ControlDao.getAllControlsinScreen(sSystemDbName, sContentDbName, 
                mActivityData.screenModel.sName);
            AppembleActivityImpl.getChildControls(mActivityData.vControls, Constants.SCREEN_ID, 
                    mActivityData.vControlsInScreenAndTitle);
            AppembleActivityImpl.getChildControls(mActivityData.vControls, Constants.TITLE_ID, 
                    mActivityData.vControlsInScreenAndTitle);
            bBeingRecreated = true;
        }
        bBeingCreated = true; isDestroyed = false;
        boolean bNetworkConnectionRequired = Utilities.parseBoolean(
            mActivityData.screenModel.mExtendedProperties.get(Constants.STR_REQUIRES_NETWORK_CONNECTION));
        if (bNetworkConnectionRequired && false == AppembleActivityImpl.isNetworkAvailable(this.getContext())) {
            AppembleActivityImpl.showAlertAndCloseScreen(this.getContext(), "Alert...", Constants.STR_NETWORK_CONNECTION_REQUIRED);
            return;
        }
        long lCreateTime = 0;
        if (Constants.isDebugMode)
            lCreateTime = System.currentTimeMillis();
        if (null == mActivityData.targetParameterValueList)
          mActivityData.targetParameterValueList = new Bundle();
        LogManager.logVerbose("Creating screen: " + mActivityData.sScreenName);
        if (null == mActivityData.vControls)
            mActivityData.vControls = ControlDao.getAllControlsinScreen(mActivityData.screenModel.sSystemDbName, 
                mActivityData.screenModel.sContentDbName, mActivityData.screenModel.sName);
        if (null == mActivityData.vControlsInScreenAndTitle) {
            mActivityData.vControlsInScreenAndTitle = new Vector<ControlModel> ();
            AppembleActivityImpl.getChildControls(mActivityData.vControls, Constants.SCREEN_ID, 
                    mActivityData.vControlsInScreenAndTitle);
            AppembleActivityImpl.getChildControls(mActivityData.vControls, Constants.TITLE_ID, 
                    mActivityData.vControlsInScreenAndTitle);
        }
        mActivityData.screenModel.fHeightInPixels = fHeight;
        mActivityData.screenModel.fWidthInPixels = fWidth;
        if (false == bBeingRecreated)
        	Action.callScreenActions(this, DynamicActivity.BEFORE_ON_CREATE_SCREEN, 
        		null, null, mActivityData.targetParameterValueList);
        
        if (null == mActivityData.vControlsInScreenAndTitle || mActivityData.vControlsInScreenAndTitle.size() == 0) {
        	finish();
            return; // log error here...
        }
        if (Constants.isDebugMode) {
            lCreateTime = System.currentTimeMillis() - lCreateTime;
            LogManager.logDebug("Screen " + mActivityData.screenModel.sName + " got created in " + lCreateTime + "ms. ");
        }
        if (false == bBeingRecreated) {
			Action.callScreenActions(this, DynamicActivity.ON_CREATE_SCREEN,
				null, null, mActivityData.targetParameterValueList);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (false == bBeingRecreated)
            Action.callScreenActions(this, Constants.ON_START_SCREEN, 
            	null, null, mActivityData.targetParameterValueList);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if (false == bBeingRecreated) { // If the screen is not being recreated, then try to close screens if needed or generate the event ON_APPLICATION_FOREGROUND
            if (AppembleActivityImpl.closeScreens(this))
                return;
            Action.callScreenActions(this, DynamicActivity.BEFORE_ON_RESUME_SCREEN, 
            	null, null, mActivityData.targetParameterValueList);
        }
        if (bBeingRecreated) {
            if (Cache.cookieSyncManager != null)
                Cache.cookieSyncManager.startSync();
        }
        // if for some reason the body layout did not get created, 
        // no point in deparameterizeing and updating controls with local or remote data
        long lUpdateTime = 0;
        if (null == bodyLayout)
            return; 
        if (Constants.isDebugMode)
            lUpdateTime = System.currentTimeMillis();
        // Call Activity.onResume
//		if (false == bBeingRecreated) // deparameterize data source only once. Have to do as deparameterized local / remote data source will be null if not done so
            AppembleActivityImpl.deparameterizeDataSources(mActivityData.screenModel, mActivityData.vControls, 
                mActivityData.targetParameterValueList, mActivityData.screenModel.sContentDbName); // substitute <parameter> with value in targetParameterValueList
        LogManager.logVerbose("Resuming screen: " + mActivityData.sScreenName + ". \r\n With Local Data Source is : " + 
                mActivityData.screenModel.sDeparameterizedLocalDataSource + 
                ". \r\n And Remote Data Source is : " + mActivityData.screenModel.sDeparameterizedRemoteDataSource);
        // Call onResume function for every view in the screen.
        AppembleActivityImpl.onScreenStateChange(Constants.TITLE_ID, titleLayout, mActivityData.vControls, 
                mActivityData.targetParameterValueList, Constants.ON_RESUME_SCREEN);
        AppembleActivityImpl.onScreenStateChange(Constants.SCREEN_ID, bodyLayout, mActivityData.vControls, 
                mActivityData.targetParameterValueList, Constants.ON_RESUME_SCREEN);
        if (bBeingCreated || bBeingRecreated || mActivityData.screenModel.bOnResumeUpdate) {
            if (bBeingCreated)
                AppembleActivityImpl.setDefaultValues(activityLayout);
            boolean bIsRemoteDataPresent = false;
            if (false == bBeingRecreated)
                bIsRemoteDataPresent = AppembleActivityImpl.isRemoteDataPresent(mActivityData.screenModel, 
                    mActivityData.vControls);
            if (!bIsRemoteDataPresent) {
                if (false == bBeingCreated && mActivityData.cursors.size() > 0) {
                    LogManager.logVerbose("Cursors should be not be present at the start of the Activity.onResume");
                    mActivityData.cursors.clear();
                }
                // fetch and update local data.
                AppembleActivityImpl.fetchLocalData(this, null, true); 
                // populate the screen with local data. 
                if (false == AppembleActivityImpl.update(this, null, null)) {
                    bBeingCreated = false;
                    return; // if the function return 0 means there is an error while executing the function.
                }
                if (Constants.isDebugMode) {
                    lUpdateTime = System.currentTimeMillis() - lUpdateTime;
                    LogManager.logDebug("Local data update: " + lUpdateTime + "ms");
                }
            } else {
	            // Fetch remote data in the background thread and then use UI thread to update the UI. It is part of lazy loading...
	            if (false == bBeingRecreated) // do not call remote data if being called because of recreation of the activity due to orientation change 
	                isNetworkAvailable = AppembleActivityImpl.updateRemoteData(this, null, true, null, isNetworkAvailable,
	                        bBeingCreated);
	            else // do not update bBeingCreated if RemoteDataIsPresent. It will be set to false in onFinish() 
	                bBeingCreated = false;
            }
        }
        if (false == bBeingRecreated)
            Action.callScreenActions(this, Constants.ON_RESUME_SCREEN, null, 
            	activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
        if (null != mActivityData && null != mActivityData.mScreenData)
          mActivityData.mScreenData.clear(); // clear any stored data.
    }

    @Override
    public void onPause() {
        super.onPause();
// Not Used Method 1: http://stackoverflow.com/questions/4414171/how-to-detect-when-an-android-app-goes-to-the-background-and-come-back-to-the-fo       
        if (Cache.cookieSyncManager != null)
            Cache.cookieSyncManager.stopSync();
        if (null == mActivityData.screenModel)
            return;
        AppembleActivityImpl.onScreenStateChange(Constants.TITLE_ID, titleLayout, mActivityData.vControls, 
                mActivityData.targetParameterValueList, Constants.ON_PAUSE_SCREEN);
        AppembleActivityImpl.onScreenStateChange(Constants.SCREEN_ID, bodyLayout, mActivityData.vControls, 
                mActivityData.targetParameterValueList, Constants.ON_PAUSE_SCREEN);
        // Cannot detect if onPause is called due to activity getting destroyed because of orientation change. 
//        if (isFinishing())
//            Action.callScreenActions(this, BEFORE_ON_DESTROY_SCREEN, null, activityLayout != null ? activityLayout : bodyLayout);
//        else
            Action.callScreenActions(this, Constants.ON_PAUSE_SCREEN, null, 
            	activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
    }

    @Override
    public void onStop() {
        super.onStop();     
        if (false == isBeingDestroyedForConfigChanges)
            Action.callScreenActions(this, Constants.ON_STOP_SCREEN, null, 
            	activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        isDestroyed = true;
        // cancel all pending remote requests if the activity is being destroyed.
        AsyncHttpClient client = HttpUtils.getAsyncHttpClient();
        if (null != client)
            client.cancelRequests(this.getContext(), true);
        if (null == mActivityData.screenModel)
            return;
        
        if (false == isBeingDestroyedForConfigChanges) {
            LogManager.logVerbose("Destroying screen: " + mActivityData.screenModel.sName);
            Action.callScreenActions(this, Constants.ON_DESTROY_SCREEN, null, 
            	activityLayout != null ? activityLayout : bodyLayout, mActivityData.targetParameterValueList);
        }
        // Call onResume function for every view in the screen.
        AppembleActivityImpl.onScreenStateChange(Constants.TITLE_ID, titleLayout, mActivityData.vControls, 
                mActivityData.targetParameterValueList, Constants.ON_DESTROY_SCREEN);
        AppembleActivityImpl.onScreenStateChange(Constants.SCREEN_ID, bodyLayout, mActivityData.vControls, 
                mActivityData.targetParameterValueList, Constants.ON_DESTROY_SCREEN);
        activityLayout = null; bodyLayout = null; titleLayout = null;
        isBeingDestroyedForConfigChanges = false; // set it to the default value. Next time it may be set to true.
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (null == mActivityData.screenModel || 0 == fWidth || 0 == fHeight)
            return null;
        Context context = this.getContext(); // getActivity();
//        DynamicFragmentActivity dynamicFragmentActivity = null;
//        Activity activity = getActivity();
//        if (null != activity && activity instanceof DynamicFragmentActivity)
//            dynamicFragmentActivity = (DynamicFragmentActivity)activity;
        
        activityLayout = new LinearLayout(context); // ActivityLayout holds both title and body layouts
        LinearLayout.LayoutParams activityParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,
            ViewGroup.LayoutParams.FILL_PARENT);
        activityLayout.setLayoutParams(activityParams);
        
        
        if (mActivityData.screenModel.iControlHeightBasedUpon != Configuration.ORIENTATION_UNDEFINED)
          mActivityData.screenModel.fHeightInPixels = mActivityData.screenModel.iControlHeightBasedUpon == Configuration.ORIENTATION_PORTRAIT ? 
              (mActivityData.screenModel.fHeightInPixels > mActivityData.screenModel.fWidthInPixels ? mActivityData.screenModel.fHeightInPixels : mActivityData.screenModel.fWidthInPixels) : 
              (mActivityData.screenModel.fHeightInPixels < mActivityData.screenModel.fWidthInPixels ? mActivityData.screenModel.fHeightInPixels : mActivityData.screenModel.fWidthInPixels);
        // Create title first
        Vector<ControlModel> vControlsInTitle = new Vector<ControlModel> ();
        AppembleActivityImpl.getChildControls(mActivityData.vControls, Constants.TITLE_ID, vControlsInTitle);
        if (vControlsInTitle.size() > 0) {
            DynamicLayout titleLayout = new DynamicLayout(context);
            if (false == DynamicLayout.generateLayout(titleLayout, Constants.TITLE_ID, 
                fWidth, fHeight, mActivityData.screenModel, mActivityData.vControls))
                return null; // Log error here
            LinearLayout.LayoutParams titleParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, 
                ViewGroup.LayoutParams.WRAP_CONTENT);
            titleLayout.setLayoutParams(titleParams);
            String sTitleBackground = mActivityData.screenModel.sTitleBackground != null ? mActivityData.screenModel.sTitleBackground :
                TableDao.getColumnValue(mActivityData.screenModel.sSystemDbName, "_screen_deck", "title_background", null);
            if (null != sTitleBackground)
                AppembleActivityImpl.setBackground(titleLayout, sTitleBackground);
            else
                titleLayout.setBackgroundColor(Color.parseColor("#0000FF")); // blue color
            activityLayout.addView(titleLayout);
            mActivityData.screenModel.fHeightInPixels -= AppembleActivityImpl.getCustomTitleBarHeight(vControlsInTitle, 
                mActivityData.screenModel.fHeightInPixels);
        } 
//        if (mActivityData.screenModel.iInitialLayout != Configuration.ORIENTATION_UNDEFINED)
//          mActivityData.screenModel.fHeightInPixels = mActivityData.screenModel.iInitialLayout == Configuration.ORIENTATION_PORTRAIT ? 
//              (mActivityData.screenModel.fHeightInPixels > mActivityData.screenModel.fWidthInPixels ? mActivityData.screenModel.fHeightInPixels : mActivityData.screenModel.fWidthInPixels) : 
//              (mActivityData.screenModel.fHeightInPixels < mActivityData.screenModel.fWidthInPixels ? mActivityData.screenModel.fHeightInPixels : mActivityData.screenModel.fWidthInPixels);
        AppearanceManager.getInstance().setInitialWindowHeight(mActivityData.screenModel.iAllowedLayouts, 
          mActivityData.screenModel.fWidthInPixels, mActivityData.screenModel.fHeightInPixels);
        // now create the body of the screen
        bodyLayout = new DynamicLayout(context);
        //if (titleLayout != null) This is too soon for titleLayout to return the height. It will always return 0.
        //  screenModel.iHeight = rScreenRect.bottom -= titleLayout.getHeight();
        // if (false == bodyLayout.generateLayout(Constants.BODY, Constants.SCREEN_ID, 
        if (false == DynamicLayout.generateLayout(bodyLayout, Constants.SCREEN_ID, mActivityData.screenModel.fWidthInPixels, 
                mActivityData.screenModel.fHeightInPixels, mActivityData.screenModel, mActivityData.vControls))
            return null; // Log error here
    
        String sBackground = mActivityData.screenModel.sBackground != null ? mActivityData.screenModel.sBackground :
            TableDao.getColumnValue(mActivityData.screenModel.sSystemDbName, "_screen_deck", "background", null);
        AppembleActivityImpl.setBackground(bodyLayout, sBackground);
        // Set the orientation
        activityLayout.setOrientation(LinearLayout.VERTICAL); // The body layout should always be below the titleLayout.

        // Add the body layout
        if (bodyLayout != null) {
            HorizontalScrollView hsv = null;
            if (mActivityData.screenModel.iScrolling == Constants.HSCROLL || mActivityData.screenModel.iScrolling == Constants.SCROLL_BOTH_WAYS) {
                hsv = new HorizontalScrollView(this.getContext());
                hsv.addView(bodyLayout);
            }
            ScrollView vsv = null;
            if (mActivityData.screenModel.iScrolling == Constants.VSCROLL || 
                    mActivityData.screenModel.iScrolling == Constants.SCROLL_BOTH_WAYS) {
                vsv = new ScrollView(this.getContext());
                if (null != hsv)
                    vsv.addView(hsv);
                else
                    vsv.addView(bodyLayout);
            }
            if (null != vsv)
            	activityLayout.addView(vsv);
            else if (null != hsv)
            	activityLayout.addView(hsv);
            else
              activityLayout.addView(bodyLayout);
        }

        return activityLayout;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("SystemDbName", mActivityData.screenModel.sSystemDbName);
        outState.putString("ContentDbName", mActivityData.screenModel.sContentDbName);
        outState.putString("ScreenName", mActivityData.screenModel.sName);
        outState.putBundle("ScreenData", AppembleActivityImpl.collectScreenData(this));
        outState.putBundle("TargetParameterValueList", mActivityData.targetParameterValueList);
        outState.putFloat("Width", fWidth);
        outState.putFloat("Height", fHeight);
        isBeingDestroyedForConfigChanges = true;
    }

    @Override
    public Context getContext() {
      return getActivity();
    }

    @Override
    public HashMap<Integer, Cursor> getCursors() {
      return mActivityData.cursors;
    }

    @Override
    public View getRootLayout() {
      return activityLayout;
    }

    @Override
    public void setActivityCreateStatus(boolean bBoolean) {
      bBeingCreated = bBoolean;
    }

    @Override
    public boolean getActivityCreateStatus() {
      return bBeingCreated;
    }

    @Override
    public boolean getActivityRecreateStatus() {
      return bBeingRecreated;
    }

    @Override
    public boolean getNetworkAvailableFlag() {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public boolean isDestroyed() {
      // TODO Auto-generated method stub
      return false;
    }

    @Override
    public void finish() {
      // TODO Auto-generated method stub
      
    }

    @Override
    public Vector<ControlModel> getAllControls() {
      return mActivityData != null ? mActivityData.vControls : null;
    }

    @Override
    public ScreenModel getScreenModel() {
      return mActivityData != null ? mActivityData.screenModel : null;
    }

    @Override
    public Bundle getTargetParameterValueList() {
      return null != mActivityData ? mActivityData.targetParameterValueList : null;
    }

    @Override
    public Activity getMyParent() {
      return getActivity();
    }

    @Override
    public ActivityData getActivityData() {
      return mActivityData;
    }
}
