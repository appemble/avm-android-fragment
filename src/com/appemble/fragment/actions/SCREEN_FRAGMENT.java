/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.fragment.actions;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.actions.ActionBase;
import com.appemble.avm.dynamicapp.AppembleActivity;
import com.appemble.avm.dynamicapp.AppembleActivityImpl;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.ControlModel;
import com.appemble.avm.models.ScreenModel;
import com.appemble.avm.models.dao.ScreenDao;
import com.appemble.fragment.activities.DynamicFragment;

public class SCREEN_FRAGMENT extends ActionBase {
	public SCREEN_FRAGMENT() {
	}

	public Object execute(Context ctx, View v, View pView, ActionModel a, Bundle tpvl) {
		super.initialize(ctx, v, pView, a, tpvl);
		AppembleActivity appembleActivity = Utilities.getActivity(context, view, parentView);
		if (null == appembleActivity) {
			LogManager.logError(Constants.MISSING_ACTIVITY);
			return Boolean.valueOf(false);
		}
		
		String sContainerName = action.getExtendedProperty("container");
		if (null == sContainerName) {
			LogManager.logError("SCREEN_FRAGMENT: The container name must be present");
			return Boolean.valueOf(false);
		}
		String sTagName = action.getExtendedProperty("tag_name");
		String sType = action.getExtendedProperty("type");
		ControlModel containerControlModel = AppembleActivityImpl.findControlModelByName(appembleActivity, sContainerName);
		if (null == containerControlModel) {
			LogManager.logError("Not a valid control name in target");
			return Boolean.valueOf(false);
		}
		
		Fragment fragment = null;
		if (appembleActivity instanceof FragmentActivity) {
			FragmentManager fm = ((FragmentActivity)appembleActivity).getSupportFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			if (sType == null || sType == "add") {
				fragment = getFragment(appembleActivity, containerControlModel);
				ft.add(containerControlModel.id, (Fragment) fragment, sType);
			}
			else if (sType == "replace") {
				fragment = getFragment(appembleActivity, containerControlModel);
				ft.replace(containerControlModel.id, (Fragment) fragment, sType);
			}
			else if (sType == "show") {
				fragment = fm.findFragmentByTag(sTagName);
				if (null != fragment)
					ft.show(fragment);
			}
			else if (sType == "hide") {
				fragment = fm.findFragmentByTag(sTagName);
				if (null != fragment)
					ft.hide(fragment);
			}
			boolean bAddToBackStack = Utilities.parseBoolean(
					action.mExtendedProperties.get("add_to_back_stack"));
			if (bAddToBackStack)
				ft.addToBackStack(null);		
			ft.commit();
		}
		
		return Boolean.valueOf(true);
	}

//	public Object executeCascadingActions(boolean bExecuteResults, Bundle targetParameterValueList) {
//		if (false == bExecuteResults)
//			return Boolean.valueOf(bExecuteResults);
//		Object object = Action.executeCascadingActions(context, view, parentView, action, targetParameterValueList, false);
//    	if (object instanceof Boolean)
//    		return (Boolean)object;
//    	else
//    		return Boolean.valueOf(false);
//	} 
	
	private Fragment getFragment(AppembleActivity appembleActivity, ControlModel containerControlModel) {
		if (action.sTarget == null) {
			LogManager.logError("Target must be present to display the fragment.");
			return null;
		}
		
		View targetView = AppembleActivityImpl.findViewInScreen(appembleActivity, containerControlModel);
		if (null == targetView) {
			LogManager.logError("Cannot find view in the screen");
			return null;
		}
		
		// load the screen model.
	    ScreenModel screenModel = ScreenDao.getScreenByName(action.sSystemDbName, action.sContentDbName, action.sTarget);
		if (null == screenModel) {
			LogManager.logError("Cannot find screen with name = " + action.sTarget);
			return null;
		}
		
		return DynamicFragment.newInstance(screenModel, containerControlModel.fWidthInPixels, 
				containerControlModel.fHeightInPixels);
	}
}