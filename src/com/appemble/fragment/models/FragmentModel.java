/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.fragment.models;

import java.util.HashMap;

import com.appemble.avm.ConfigManager;
import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.models.ActionModel;
import com.appemble.avm.models.dao.ActionDao;
import com.appemble.fragment.models.dao.FragmentDao;

public class FragmentModel {
	public int id;
	public String sName;
	public String sTitle;
	public int iScreenType;
	public String sIcon;
	public int iMenuOrder;
	public String sMenuName;
	public int iDimensionType;
	public float fWidth;
	public float fWidthInPixels;
	public float fHeight;
	public float fHeightInPixels;
	public int iScrolling;
	public String sRemoteDataSource;
	public String sLocalDataSource;
	public int iRemoteRequestType;
	public String sBackground;
	public String sTitleBackground;
	public String sInitialFocus;
	public boolean bActionYN;

	public String sDeparameterizedRemoteDataSource;
	public String sDeparameterizedLocalDataSource;
	public boolean bMarkDeleted;
	public boolean bOnResumeUpdate;
	public String sMisc1;
	public String sMisc2;
	public String sTabGroupName;
	public HashMap <String, String> mExtendedProperties;

	public String sSystemDbName;
	public String sContentDbName;
 	public ConfigManager.ActivityMetaData metaData;
 	public boolean bRemoteDataSaveLocally;
 	public float fTabBarHeight = 0;
 	public int iDefaultTitleBar = -1;
 	
 	public FragmentModel() {
		mExtendedProperties = new HashMap<String, String>();
	}
 	
	public void setType(String sScreenType) {
		// iScreenType = Constants.SCREEN;
		if (sScreenType == null || sScreenType.length() == 0) {
			metaData = ConfigManager.getInstance().getActivityMetaData("SCREEN");
			iScreenType = metaData.id;
			return;
		}
		metaData = ConfigManager.getInstance().getActivityMetaData(sScreenType);
		if (null == metaData) {
			LogManager.logWTF("Cannot instantiate the fragment: " + sName + ". The defintion is missing in the configuration file for control type = " + sScreenType);
			return;
		}
		iScreenType = metaData.id;
	}

	public void setDimensionType(String dimensionType) {
		if (null == dimensionType || dimensionType.length() == 0)
			return;
		if (dimensionType.equalsIgnoreCase("PERCENTAGE"))
			iDimensionType = Constants.PERCENTAGE;
		else if (dimensionType.equalsIgnoreCase("PIXELS"))
			iDimensionType = Constants.PIXELS;
	}

	public void setScrolling(String sScrolling) {
		iScrolling = 0;
		if (null == sScrolling || sScrolling.length() == 0)
			return;
		if (sScrolling.equalsIgnoreCase("HORIZONTAL"))
			iScrolling = Constants.HSCROLL;
		else if (sScrolling.equalsIgnoreCase("VERTICAL"))
			iScrolling = Constants.VSCROLL;
		else if (sScrolling.equalsIgnoreCase("SCROLL_BOTH_WAYS"))
			iScrolling = Constants.SCROLL_BOTH_WAYS;
	}

	public void setRemoteRequestType(String sRequestType) {
		if (null == sRequestType || sRequestType.length() == 0)
			return;
		if (sRequestType.equalsIgnoreCase("POS"))
			iRemoteRequestType = Constants.POST;
		else
			iRemoteRequestType = Constants.GET;
	}
	
	public String getClassName() {
		if (null == metaData) {
			LogManager.logWTF("Cannot instantiate the fragment: " + sName + ". The defintion is missing in the configuration file for control type = " + iScreenType);
			return null;
		}
		return metaData.sClass;
	}
	
	public ActionModel[] getActions(int iEventType) {
		return getActions(iEventType, true);
	}
	
	public ActionModel[] getActions(int iEventType, boolean bScreenOnly){
		String sEventType = ActionModel.getEventString(iEventType);
		ConfigManager.EventMetaData event = ConfigManager.getInstance().getEventMetaData(iEventType);
		if (null == event) {
			LogManager.logWTF("The config file(s) does not contain an event:" + iEventType);
			return null;
		}
		ActionModel[] a = null;
		if (null == event.sActionName)
			a = ActionDao.getActionsForScreen(sSystemDbName, sContentDbName, id, sEventType, bScreenOnly);
		else {
			ActionModel action = new ActionModel(sSystemDbName, sContentDbName);
			action.setActionName(event.sActionName);
			action.sTarget = event.sTarget;
			action.vInputParameters = event.vInputParameter;
			action.vInputParameters = event.vTargetParameter;
			a = new ActionModel[1];
			a[0] = action;
		}
		return a;
	}

	public String getExtendedProperty(String sKey) {
		if (null == mExtendedProperties || null == sKey)
			return null;
		return mExtendedProperties.get(sKey);
	}
	
    public static FragmentModel getFragmentModel(String sSystemDbName, String sContentDbName, String sFragmentName) {
    	if (null == sSystemDbName || null == sFragmentName) {
			LogManager.logError("Cannot instantiate the fragment. The system database is not found: " + sSystemDbName);
    		return null;
    	}
		FragmentModel fragmentModel = FragmentDao.getFragmentByName(sSystemDbName, sContentDbName, sFragmentName);
		if (null == fragmentModel) {
			LogManager.logError("Cannot instantiate the fragment: " + sFragmentName + ". Please check the fragment name.");
			return null;
		}
    	return fragmentModel;
    }
    
    
}