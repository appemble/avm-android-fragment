/*
 * Copyright (C) 2012 The App Virtual Machine
 *
 * Licensed under the Appemble LLC ("Appemble") License, Version 1.0 
 * (the "License"); You may obtain a copy of the License at
 *     http://www.appemble.com/licenses/LICENSE-1.0
 * If you do not agree with these terms, please do not use, install, 
 * modify this Appemble software and you may want to destroy all copies 
 * of this software. 
 *
 * This software is provided by Appemble on an "AS IS" basis. Appemble
 * makes no warranties, express or implied, including without limitation
 * the implied warranties of non-infringement, merchantability and fitness
 * for a particular purpose, regarding the Appemble software or its use 
 * for a operation in conjunction with your products or standalone.
 */

package com.appemble.fragment.models.dao;

import java.util.HashMap;
import java.util.Vector;

import android.database.Cursor;
import android.database.SQLException;

import com.appemble.avm.Constants;
import com.appemble.avm.LogManager;
import com.appemble.avm.Utilities;
import com.appemble.avm.models.dao.TableDao;
import com.appemble.fragment.models.FragmentModel;

public class FragmentDao {

	public static Vector <FragmentModel> getFragmentGroup(String sSystemDbName, String sContentDbName, String sGroupName) {
		if (null == sGroupName) {
			LogManager.logError("Unable to get the TABS screen as the Tab Group Name is missing");
			return null;
		}
	    String sSQL = "SELECT * FROM _screen WHERE " + Constants.sDefaultFilter + " and tab_group_name =  ? and menuOrder >= 0 ORDER BY menuOrder";
		String[] bindArgs = new String[]{sGroupName};
		Vector<FragmentModel> vFragments = getFragments(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vFragments;
	}

	public static FragmentModel getFragmentByName(String sSystemDbName, String sContentDbName, String screenName) {
		if (null == sSystemDbName || null == sContentDbName || null == screenName)
			return null;
	    String sSQL = "SELECT * FROM _screen WHERE " + Constants.sDefaultFilter + " and name =  ?";
		String[] bindArgs = new String[]{screenName};
		Vector<FragmentModel> vFragments = getFragments(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return (null != vFragments && vFragments.size() > 0) ? vFragments.get(0) : null;
	}

	public static FragmentModel getFragmentById(String sSystemDbName, String sContentDbName, int id) {
		if (null == sSystemDbName || null == sContentDbName || 0 == id)
			return null;
	    String sSQL = "SELECT * FROM _screen WHERE " + Constants.sDefaultFilter + " and _id =  ?";
		String[] bindArgs = new String[]{Integer.toString(id)};
		Vector<FragmentModel> vFragments = getFragments(sSystemDbName, sContentDbName, sSQL, bindArgs);
		return vFragments.get(0);
	}

	static Vector <FragmentModel> getFragments(String sSystemDbName, String sContentDbName, String sSQL, String[] bindArgs) {
		if (null == sSystemDbName || null == sContentDbName || null == sSQL)
			return null;
		FragmentModel newFragment = null;
	    Cursor cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
		Vector <FragmentModel> vFragments = null; 
		try {
			// here set the limit to 1
	        while (null != cur && cur.moveToNext()) {
				if (null == vFragments)
					vFragments = new Vector <FragmentModel>();
	        	newFragment = getFragmentModel(cur, sSystemDbName, sContentDbName);
	        	vFragments.add(newFragment);
	        }
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Error in fetching a screen: " + sSQL);
		} finally {
			if (null != cur)
		        cur.close();
		}
		return vFragments;
	}

	public static FragmentModel getFragmentModel(Cursor cur, String sSystemDbName, String sContentDbName) {
		if (null == cur)
			return null;
		int iColumnIndex = -1;
    	FragmentModel newFragment=new FragmentModel();
    	newFragment.id = (cur.getInt(cur.getColumnIndex("_id")));
    	newFragment.setType(cur.getString(cur.getColumnIndex("screen_type")));
    	newFragment.sName = (cur.getString(cur.getColumnIndex("name")));
    	newFragment.sTitle = (cur.getString(cur.getColumnIndex("title")));
    	newFragment.sIcon = (cur.getString(cur.getColumnIndex("icon")));
    	newFragment.iMenuOrder = (cur.getInt(cur.getColumnIndex("menuOrder")));
    	newFragment.sMenuName = (cur.getString(cur.getColumnIndex("menuName")));
    	newFragment.fWidth = (cur.getFloat(cur.getColumnIndex("width")));
    	newFragment.fHeight = (cur.getFloat(cur.getColumnIndex("height")));
    	newFragment.setScrolling(cur.getString(cur.getColumnIndex("scroll")));
    	newFragment.setDimensionType(cur.getString(cur.getColumnIndex("dimension_type")));
    	newFragment.sTitleBackground = (cur.getString(cur.getColumnIndex("title_background")));
    	newFragment.sBackground = (cur.getString(cur.getColumnIndex("background")));
    	newFragment.sRemoteDataSource = (cur.getString(cur.getColumnIndex("remote_data_source")));
    	newFragment.setRemoteRequestType(cur.getString(cur.getColumnIndex("remote_request_type")));
    	newFragment.sLocalDataSource = (cur.getString(cur.getColumnIndex("local_data_source")));
    	newFragment.sInitialFocus = (cur.getString(cur.getColumnIndex("initial_focus")));
    	newFragment.bActionYN = (cur.getInt(cur.getColumnIndex("action_yn")) == 1);
    	newFragment.bMarkDeleted = (cur.getInt(cur.getColumnIndex("mark_deleted")) == 1);
    	if ((iColumnIndex = cur.getColumnIndex("on_resume_update")) > -1)
    		newFragment.bOnResumeUpdate = (cur.getInt(iColumnIndex) == 1);
    	if ((iColumnIndex = cur.getColumnIndex("misc1")) > -1)
    		newFragment.sMisc1 = (cur.getString(iColumnIndex));
    	if ((iColumnIndex = cur.getColumnIndex("misc2")) > -1)
    		newFragment.sMisc2 = (cur.getString(iColumnIndex));
    	if ((iColumnIndex = cur.getColumnIndex("tab_group_name")) > -1)
    		newFragment.sTabGroupName = (cur.getString(iColumnIndex));
	    if ((iColumnIndex = cur.getColumnIndex("extended_properties")) > -1)
	    	newFragment.mExtendedProperties = Utilities.getNameValuePairMap(cur.getString(
	    			cur.getColumnIndex("extended_properties")));
	    if (null == newFragment.mExtendedProperties)
	    	newFragment.mExtendedProperties = new HashMap<String, String>(); 
    	newFragment.bRemoteDataSaveLocally = Utilities.parseBoolean(
    			newFragment.mExtendedProperties.get("remote_data_save_locally"));
    	String sTabBarHeight = newFragment.mExtendedProperties.get("tab_bar_height");
    	try {
    	if (null != sTabBarHeight)
    		newFragment.fTabBarHeight = Utilities.getDimension(sTabBarHeight.trim(), 0);
    	} catch(NumberFormatException nfe) {
    		LogManager.logError(nfe, "Unable to parse tar_bar_height");
    	}
	    String sDefaultTitleBar = newFragment.mExtendedProperties.get("default_title_bar");
	    if (null != sDefaultTitleBar)
	    	newFragment.iDefaultTitleBar = Utilities.parseBoolean(sDefaultTitleBar) ? 1 : 0; 
    	newFragment.sSystemDbName = (sSystemDbName);
    	newFragment.sContentDbName = (sContentDbName);
   		return newFragment;
	}

	public static String getFragmentType(String sSystemDbName, String screenName) {
		String screenType=null; String sSQL = null;

        Cursor cur = null;
		try {
		    sSQL = "SELECT screen_type FROM _screen WHERE " + Constants.sDefaultFilter + " and name =  ?";
			String[] bindArgs = new String[]{screenName};
		    cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
	        if (null != cur && cur.moveToNext())
	        	screenType=cur.getString(cur.getColumnIndex("screen_type"));
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + sSQL);
		} finally {
			if (null != cur)
				cur.close();
		}
		return screenType;
	}
	
	public static String getFragmentName(String sSystemDbName, int screenID) {
		if (screenID <= 0)
			return null;
		String screenName=""; Cursor cur = null;String sSQL = null;
		try {
		    sSQL = "SELECT name FROM _screen WHERE " + Constants.sDefaultFilter + " and _id =  ?";
			String[] bindArgs = new String[]{Integer.toString(screenID)};
		    cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
	        if (null != cur && cur.moveToNext())
	        	screenName=cur.getString(cur.getColumnIndex("name"));
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + sSQL);
		} finally {
			if (null != cur)
				cur.close();
		}
		return screenName;
	}
	
	public static int getFragmentID(String sSystemDbName, String sContentDbName, String screenName) {
		int screenID=1; Cursor cur = null; String sSQL = null;
		try {
		    sSQL = "SELECT _id FROM _screen WHERE " + Constants.sDefaultFilter + " and name = ?";
			String[] bindArgs = new String[]{screenName};
		    cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, bindArgs);
	        if (null != cur && cur.moveToNext())
	        	screenID=cur.getInt(cur.getColumnIndex("_id"));
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + sSQL);
		} finally {
			if (null != cur)
		        cur.close();
		}
		return screenID;
	}

	public static String getFirstViewName(String sSystemDbName) {
		Cursor cur = null; String screenName = null, sSQL = null;
		try {
		    sSQL = "SELECT name FROM _screen WHERE " + Constants.sDefaultFilter + " and id = 1";
		    cur = (Cursor)TableDao.executeSQL(sSystemDbName, sSQL, null);
			if (null != cur && cur.moveToFirst())
				screenName = cur.getString(cur.getColumnIndex("name"));
		} catch (SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + sSQL);
		} finally {
			if (null != cur)
				cur.close();
		}
		return screenName;
	}

	public static HashMap<String, String> get1Row(String sSystemDbName, String screenTableName, String filter) {
		Cursor cur = null;
		HashMap<String, String> fieldListHashMap=new HashMap<String, String>();
		try{
			if(filter==null)
				filter="1=1";
			cur = TableDao.selectData(sSystemDbName, screenTableName, null, Constants.sDefaultFilter + " and " + filter, 
					null, null, null, null);
			if (null != cur && cur.moveToNext()) {
				int iColumnCount = cur.getColumnCount();
				for(int i = 0; i < iColumnCount; i++) {
					fieldListHashMap.put(cur.getColumnName(i), cur.getString(i));	
				}
			}
		} catch(SQLException sqle) {
			LogManager.logError(sqle, "Unable to fetch a screen: " + filter);
		} finally {
			if (null != cur)
				cur.close();			
		}
		return fieldListHashMap;
	}
}
